# Reto-DevOps CLM
![CLM Consoltores](./img/clm.png)

Este reto fue diseñado para mostrar tus habilidades DevOps. Este repositorio contiene una aplicación simple en NodeJs.

## El reto comienza aquí

### Reto 1. Dockerize la aplicación
![docker](./img/nodedoker.jpg)

**. El reto es:
1. Construir la imagen más pequeña que pueda. Escribe un buen Dockerfile :)
•	A continuación, se muestra el Dockerfile solicitado:
```bash
FROM node:10-alpine
WORKDIR /usr/src/app
COPY ["package.json", "package-lock.json*", "./"]
RUN npm install
COPY . .
EXPOSE 3000
CMD ["node", "index.js"]

```

2. Ejecutar la app como un usuario diferente de root.
   • Pendiente

### Reto 2. Docker Compose
![compose](./img/docker-compose.png)

1. Nginx que funcione como proxy reverso a nuesta app Nodejs
•	El siguiente nginx muestra la configuración para el reverse-proxy
```bash
http {  
   server {
        listen 80;
        server_name localhost;

        location / {
            auth_basic off;
            proxy_pass http://node-js:3000;
        }
    } 
}

```

2. Asegurar el endpoint /private con auth_basic
•	El siguiente nginx muestra la configuración para /private con auth_basic
```bash
        location /private {
            auth_basic "Reto DevOps Basic Auth";
            auth_basic_user_file /etc/nginx/.auth;
            proxy_pass http://node-js:3000;
        }

```
3. Habilitar https y redireccionar todo el trafico 80 --> 443
•	Se muestra parte de la configurafión para generar el redireccionamiento
```bash
  server {
        listen 443 ssl http2;
        listen [::]:443 ssl http2;
        server_name localhost 127.0.0.1;

        server_tokens off;

        ssl_certificate     /etc/nginx/localhost.crt;
        ssl_certificate_key /etc/nginx/localhost.key;
```
### Reto 3. Probar la aplicación en cualquier sistema CI/CD
![cicd](./img/cicd.jpg)

Utilizando Dockerhub, se subió la imagen utilizando la cuenta jbelmar22

docker push jbelmar22/reto_devops:tagname

Para realizar la ejecución del pipeline en Gitlab, se creó el siguiente repositorio:
https://gitlab.com/jesus.belmar/repo_devops
Donde se configuran las siguientes variables para ser utilizadas por el archivo: .gitlab-ci.yml de forma paramétrica:
•	IMAGE_NAME
•	USERNAME
•	PASSWORD
![k8s](./img/variables.png)

Se definieron los siguientes stages, dentro del pipeline:

```bash
stages:
  - build-cache  # Se administrant las dependencias
  - unit-test    # Se ejecutan las pruebas
  - build        # Se construye la imagen y se realiza un push a docker

```

A continuación, se muestra una ejecución exitosa del pipeline en Gitlab

![k8s](./img/pipeline.png)

### Reto 4. Deploy en kubernetes
![k8s](./img/k8s.png)

Para utilizar Helm Chart, se definen 2 archivos de configuración para evidenciar que se pueden realizar distintas configuraciones dependiendo del ambiente:
Config (directorio):
•	values-dev.yaml
•	values-prod.yaml

En estos archivos de realiza la configuración de la aplicación
A continuación, podemos observar un ejemplo de como se definen los límites de CPU y memoria, posteriormente se configura el autoscaling entre 1 y 5 replicas

La siguiente es una muestra de como se define la plantilla a utilizar
```bash
{{- if .Values.autoscaling.enabled }}
apiVersion: autoscaling/v2beta1
kind: HorizontalPodAutoscaler
metadata:
  name: {{ template "nginx-reverse-proxy.fullname" . }}
  labels:
    app: {{ template "nginx-reverse-proxy.name" . }}
    chart: {{ template "nginx-reverse-proxy.chart" . }}
    release: {{ .Release.Name }}
    heritage: {{ .Release.Service }}
spec:                              
  scaleTargetRef:
    apiVersion: apps/v1beta1
    kind: Deployment
    name:  {{ template "nginx-reverse-proxy.fullname" . }}
  minReplicas: {{ .Values.autoscaling.minReplicas }}
  maxReplicas: {{ .Values.autoscaling.maxReplicas }}
  metrics:
    - type: Resource
      resource:
        name: cpu
        targetAverageUtilization: {{ .Values.autoscaling.targetCPUAverageUtilization }}    
{{- end -}}

```
Esto es un ejemplo de la implementación del template de HPA
```bash
autoscaling:
  enabled: true
  minReplicas: 1
  maxReplicas: 5
  targetCPUAverageUtilization: 75

```
### Reto 5. Construir Chart en helm y manejar trafico http(s)
![helm](./img/helm-logo-1.jpg)

Realmente el pan de cada día es crear, modificar y usar charts de helm. Este reto consiste en:

1. Diseñar un chart de helm con nginx que funcione como proxy reverso a nuesta app Nodejs
El diseño del helm chart está dentro de la carpeta deployment, el cual tiene las siguientes carpetas:
* config: contiene la implementación del template
* nginx-reverse-proxy: se define el template

Para desplegar este template ejecutamos el siguiente comando
```bash
helm upgrade --install --namespace default --values deployment/config/values-dev.yaml reto-devops deployment/nginx-reverse-proxy/
```
2. Asegurar el endpoint /private con auth_basic. * Pendiente
3. Habilitar https y redireccionar todo el trafico 80 --> 443. * Pendiente

### Reto 6. Terraform
![docker](./img/tf.png)

Se muestra la creación del recurso en terraform
```bash
resource "kubernetes_cluster_role" "pod_reader" {
  metadata {
    name        = "pod_reader"
    namespace   = "default"
  }

  rule {
    api_groups = [""]
    resources  = ["pods"]
    verbs      = ["get", "list", "watch"]
  }
}
```

### Reto 7. Automatiza el despliegue de los retos realizados
![docker](./img/make.gif)

Se crea un archivo config-certificates.mk
```bash
make -f config-certificates.mk generate-certificates
```
