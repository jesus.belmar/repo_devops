resource "kubernetes_cluster_role" "pod_reader" {
  metadata {
    name        = "pod_reader"
    namespace   = "default"
  }

  rule {
    api_groups = [""]
    resources  = ["pods"]
    verbs      = ["get", "list", "watch"]
  }
}